## Preparation

- [Create sudoer user](doc/prep/create_user.md)
- [Disable root ssh](doc/prep/disable_ssh_root.md)

## Installation

-  [Install from master](doc/inst/from_master.md)

## Usage

- [vanilla configuration](doc/usage/vanilla_config.md)

## Development

- [first module](doc/dev/first_module.md)
- [first app](doc/dev/first_app.md)
- [fire and react to events](doc/dev/first_event.md)
- [configure module](doc/dev/configure_module.md)
